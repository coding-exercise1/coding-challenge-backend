package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal

data class ProductUpdate(
    val name: String,
    val description: String,
    val price: BigDecimal
) {

    fun ProductEntity.toProductResponse() = ProductUpdate(
        name = name,
        description = description ?: "",
        price = price,
    )

}
