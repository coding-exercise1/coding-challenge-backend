package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductUpdate
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/product/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if (product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)

        }
    }

    @GetMapping("/products")
    fun findAllProductsBySku(@RequestParam skus: String): ResponseEntity<List<ProductResponse>> {
        logger.info("Request for product $skus")
        return try {
            if (skus.isNotEmpty()) {
                val productList = productService.findProductsBySkus(skus.split(","))
                ResponseEntity.ok(productList)

            } else {
                ResponseEntity.notFound().build()
            }
        } catch (e: Exception){
            ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
        }

    }

    @PostMapping("/product")
    fun addProduct( @RequestBody product:ProductEntity) : ResponseEntity<ProductEntity> {
       return  try {
              ResponseEntity.ok(productService.addProduct(product))

        } catch (ex: RuntimeException) {
            ResponseEntity.badRequest().build()
        }
    }


    @PutMapping("/product/{sku}")
    fun updateProductBySku(@PathVariable sku: String, @RequestBody newProduct: ProductUpdate): ResponseEntity<Optional<ProductEntity>> {
      return try {
          ResponseEntity.ok(productService.updateProduct(sku, newProduct))
      } catch (ex: Exception){
          ResponseEntity.status(HttpStatus.NOT_FOUND).build()
      }

    }





}
