package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductUpdate
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.*

@Service
class ProductService(private val productRepository: ProductRepository) {


    fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findBySku(sku)
    }

    fun findProductsBySkus(skus: Iterable<String>): List<ProductResponse> {
        return productRepository.findBySkuIn(skus)
    }

    fun addProduct(product: ProductEntity): ProductEntity {
        if (productRepository.existsById(product.sku)) {
            return throw RuntimeException("Product with Sku: ${product.sku} already exists")

        }
        return productRepository.save(product)
    }


    fun updateProduct(sku: String, newProduct: ProductUpdate): Optional<ProductEntity> {

        val getProduct = productRepository.findById(sku)

        return if (getProduct.isPresent) {
            getProduct.map { currentProduct ->
                val updateProduct: ProductEntity =
                    currentProduct
                        .copy(
                            name = newProduct.name,
                            description = newProduct.description,
                            price = newProduct.price,
                        )
                productRepository.save(updateProduct)
            }


        } else throw Exception("Product with Sku:$sku doesn't exist")
    }


    }

