package de.imedia24.shop.controller

import com.fasterxml.jackson.databind.ObjectMapper
import de.imedia24.shop.domain.product.ProductUpdate
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.*
import java.math.BigDecimal


@SpringBootTest
@AutoConfigureMockMvc
 class ProductControllerTest @Autowired constructor(
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper
) {


     @Nested
    @DisplayName("GET /products")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetProductsList {
            @Test
            fun `should return list of products by skus`() {

                mockMvc.get("/products?skus=1010,2020")
                    .andDo { print() }
                    .andExpect {
                        status { isOk() }
                        content { contentType(MediaType.APPLICATION_JSON) }
                        jsonPath("$[0].sku") { value("1010") }
                        jsonPath("$[1].sku") { value("2020") }
                    }
                }
    }

    @Nested
    @DisplayName("PUT /product")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class UpdateProduct {

        private val updateProduct = ProductUpdate("nameChanged", "descriptionChanged", BigDecimal(111))
        @Test
        fun `should update an existing product`() {

            val updateProductRequest = mockMvc.put("/product/1010") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(updateProduct)
            }

            updateProductRequest
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(updateProduct))
                    }
                }
        }

        @Test
        fun `should return BAD REQUEST if product doesn't exists`() {


            val updateProductRequest = mockMvc.put("/product/121212") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(updateProduct)
            }

            // then
            updateProductRequest
                .andDo { print() }
                .andExpect { status { isNotFound() } }
        }


    }}













