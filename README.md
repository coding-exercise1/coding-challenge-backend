# iMedia24 Coding challenge

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Official Kotlin documentation](https://kotlinlang.org/docs/home.html)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/#build-image)
* [Flyway database migration tool](https://flywaydb.org/documentation/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

## Usage

### Prerequisites
* Docker and Docker compose installed on your machine

### Running the application in a Docker container
1. Clone the repository
2. Open a terminal and navigate to the root directory of the cloned repository
3. Put the following command:
```
docker-compose up -d
```
This will create and start the containers as specified configuration file.

4. After the containers are started, you can check their status with following command 

```
docker ps
```
5. Use the following command to send an HTTP GET request to the application
```
curl localhost:9000/product/1010
```
Note: By default, it will create two products with SKUs 1010 and 2020

